package cz.clovekvtisni.coordinator.domain;

public enum NotificationType {
    ASSIGN, UNASSIGN
}
